package upm.daw;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import java.util.*;

@Controller
public class AppController {




  @RequestMapping("/login")
  public ModelAndView index(){

    return new ModelAndView("login");
  }


  @RequestMapping(value = "/dos", method = RequestMethod.POST)
  public ModelAndView test(/*@ModelAttribute("mytext") ContactForm texts,*/
                           @ModelAttribute("prueba") Untexto text,
                           BindingResult result){
    System.out.println("que voy");

    System.out.println(result.hasErrors());

    System.out.println(result.getAllErrors().toString());

    System.out.println(text.getUntexto());

    for (Simpletexto texto: text.getUntexto()
         ) {
      System.out.println(texto.getText());
    }

    return new ModelAndView("login");
  }
/*
  @Secured({"ROLE_USER"})
  @RequestMapping("/")
  @Cacheable(value="principal",condition="T(org.springframework.security.core.context.SecurityContextHolder).getContext()" +
          ".getAuthentication().getAuthorities().toString().contains(\"ROLE_ADMIN\")")
  public ModelAndView principal(@PathParam("nombre") String nombre,@PathParam("pag") String pag){

    boolean admin = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().contains("ROLE_ADMIN");
    int pagi=1;
    int itemsPorPagina=16;
    pagi=Integer.parseInt((pag!=null?pag:"1"));


    Iterable<Pelicula> peliculas = peliculaRepository.findByNombrePeliculaLike("%"+(nombre!=null?nombre:"")+"%");
    Iterator<Pelicula> ite=peliculas.iterator();
    List<Pelicula> listado=new ArrayList<Pelicula>();


    for(int i=0;i<(pagi-1)*itemsPorPagina;i++){
      ite.next();
    }

    RestAdapter adapter = new RestAdapter.Builder().setEndpoint( "https://api.themoviedb.org" ).build();
    ServicioBusqueda service = adapter.create(ServicioBusqueda. class);



    for (int i=itemsPorPagina*(pagi-1);i<itemsPorPagina*pagi && ite.hasNext();i++) {
      Pelicula p=ite.next();

      MovieList res;

      res=service.getList(p.getNombre());


      if(res!= null && res.getTotalResults()!=0) {

        if (res.getResults().iterator().next().getPosterPath()!=null && (p.getUrlPortada() == null || p.getUrlPortada() == "" || p.getUrlPortada().length() == 1 || p.getUrlPortada().length() == 0)) {
          p.setUrlPortada("http://image.tmdb.org/t/p/w342" +res.getResults().iterator().next().getPosterPath());
        }

      }
      listado.add(p);
    }

    String prevclass=(pagi!=1?"":"disabled");
    String nextclass=(ite.hasNext()?"":"disabled");
    String current=""+pagi;
    return new ModelAndView("principal").addObject("peliculas",listado)
            .addObject("prevclass",prevclass)
            .addObject("nextclass",nextclass)
            .addObject("current",current)
            .addObject("filter",nombre)
            .addObject("admin",admin);
  }




  @Secured({ "ROLE_ADMIN"})
  @RequestMapping(value="/administrarUsuarios/nuevo")
  public ModelAndView processUser(HttpSession session, @RequestParam(value="user", required = false) String user,
                                  @RequestParam(value="email", required = false) String email,
                                  @RequestParam(value="password", required = false)String password,
                                  @RequestParam(value="intento", required = false) String intento) {

    String mensaje="";

    // Verificamos si intentado introducir datos
    if(intento!=null) {
      mensaje="Usuario procesado correctamente.";
      GrantedAuthority[] roles = {new SimpleGrantedAuthority("ROLE_USER")};

      //Verificamos si todos los campos han sido completados
      if(email!=null && email!="" && password!=null && password!="" && user!=null && user!="") {

        try {
          userRepository.save(new User(user, password, email, Arrays.asList(roles)));

          //Salta error si el usuario ya existe
        } catch (NonTransientDataAccessException e) {
          if (e.getCause() instanceof ConstraintViolationException) {
            mensaje = "Usuario procesado con error: el nombre de usuario ya existe";
          } else throw e;
        }

      }
      else{
        mensaje="Usuario procesado con error, todos los campos son obligatorios.";
      }
    }
    return new ModelAndView("nuevoUser")
            .addObject("mensaje",mensaje)
            .addObject("admin",SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().contains("ROLE_ADMIN"));
  }


  @Secured({ "ROLE_ADMIN"})
  @RequestMapping(value="/administrarUsuarios/buscar")
  public ModelAndView buscarUsuario(@RequestParam(value="user", required = false) String user){

    Boolean encontrado=false;
    String mensaje="";
    String id="";

    // Verificamos si intentado introducir datos
    if(user!=null){
      User usuario=userRepository.findByUser(user);

      // Caso en el que ha encontrado un usuario
      if(usuario!=null){
        mensaje="Usuario encontrado con id: "+usuario.getId();
        encontrado=true;
        id=""+usuario.getId();
      }
      else{
        mensaje="El usuario con nombre: \""+user+"\" no existe.";
      }
    }


    return new ModelAndView("buscarUser")
            .addObject("mensaje",mensaje)
            .addObject("encontrado",encontrado)
            .addObject("id",id)
            .addObject("admin",SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().contains("ROLE_ADMIN"));

  }



  @Secured({ "ROLE_ADMIN"})
  @RequestMapping(value="/administrarUsuarios/{id}/modificar")
  public ModelAndView modificarUsuario(@PathVariable("id") String id,
                                       @RequestParam(value="user", required = false) String user,
                                       @RequestParam(value="password", required = false) String password,
                                       @RequestParam(value="email", required = false) String email,
                                       @RequestParam(value="intento", required = false) String intento) {

    String mensaje="";
    long ident=0;
    boolean valido=true;
    User usuario=null;


    //Verificamos que el id sea un numero
    try {
      ident = (long)Integer.parseInt(id);
    } catch (NumberFormatException e) {
      mensaje = "Error: El id introducido no es un numero.";
      valido=false;
    }


    //Si el id es un numero
    if(valido) {

      usuario = userRepository.findOne(ident);

      //Verificamos si el usuario no existe
      if(usuario==null){
        mensaje="El id no corresponde a ningun usuario.";
      }

      //Verificamos que el usuario existe y que el usuario ha intentado introducir daos
      if (usuario!=null && intento != null) {
        mensaje = "Usuario modificado correctamente.";

        //Verificamos que todos los compos hayan sido completados menos el password
        if (email != null && email != ""  && user != null && user != "") {

          try {
            usuario.setEmail(email);
            usuario.setUser(user);

            // Verificamos si el usuario ha introducido un nuevo password
            if(password != null && password != ""){
              usuario.setPassword(password);
            }
            userRepository.save(usuario);

            //Caso en el que el nuevo nombre de usuario ya existe
          } catch (NonTransientDataAccessException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
              mensaje = "Usuario procesado con error: el nombre de usuario ya existe";
            } else throw e;
          }
        } else {
          mensaje = "Usuario modificado con error, el nombre e email son obligatorios.";
        }
      }
    }
    return new ModelAndView("modificarUser")
            .addObject("id",id)
            .addObject("mensaje",mensaje)
            .addObject("encontrado",usuario!=null)
            .addObject("usuario",usuario)
            .addObject("admin",SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().contains("ROLE_ADMIN"));
  }

  @Secured({ "ROLE_ADMIN"})
  @RequestMapping(value="/administrarUsuarios/{id}/eliminar")
  public ModelAndView eliminarUsuario(@PathVariable("id") String id) {

    String mensaje="";
    long ident=0;
    boolean valido=true;

    //Verificamos que el id sea un numero
    try {
      ident = (long)Integer.parseInt(id);
    } catch (NumberFormatException e) {
      mensaje = "Error: El id introducido no es un numero.";
      valido=false;
    }

    //Si es un id valido
    if(valido) {

      //Si el id corresponde a un usuario, borramos
      if(!userRepository.exists(ident)){
        mensaje="El id \""+id+"\" no corresponde a ningun usuario.";
      }else{
        userRepository.delete(ident);
        mensaje = "Usuario eliminado correctamente.";
      }
    }
    return new ModelAndView("eliminarUser")
            .addObject("mensaje",mensaje)
            .addObject("admin",SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().contains("ROLE_ADMIN"));
  }


  @CacheEvict(value ="principal",allEntries=true)
  @Secured({ "ROLE_ADMIN"})
  @RequestMapping(value="/administrarPeliculas/nueva")
  public ModelAndView processNewMovie(HttpSession session, @RequestParam(value="nombrePelicula", required = false) String nombrePelicula,
                                      @RequestParam(value="urlPelicula", required = false) String urlPelicula,
                                      @RequestParam(value="sinopsis", required = false) String sinopsis,
                                      @RequestParam(value="ano", required = false) String ano,
                                      @RequestParam(value="director", required = false) String director,
                                      @RequestParam(value="reparto", required = false) String reparto,
                                      @RequestParam(value="urlPortada", required = false) String urlPortada,
                                      @RequestParam(value="valoracion", required = false) String valoracion,
                                      @RequestParam(value="intento", required = false) String intento) {

    String mensaje = "";
    String color = "color:#07BD1D";

    //Verificamos que el usuario ha intentado introducir datos
    if(intento!=null) {
      mensaje = "Pelicula "+nombrePelicula+" introducida correctamente.";

      //Verificamos si ha intentado introducir año
      if (ano == "")
        ano = null;
      else {

        //Si ha intentado introducir año, verificamos que sea un numero valido
        try {
          Integer.parseInt(ano);
        } catch (NumberFormatException e) {
          mensaje = "Error: El campo año introducido no es un numero.";
          color = "color:#D20F0F";
        }
      }

      try {
        // Verificamos si se han introducido los campos obligatorios
        if (nombrePelicula == "" || urlPelicula == "") {
          mensaje = "Error: No se han introducido los campos obligatorios.";
          color = "color:#D20F0F";
        } else {

          //Rellenamos con null los campos vacios
          if (sinopsis == "")
            sinopsis = null;
          if (director == "")
            director = null;
          if (reparto == "")
            reparto = null;
          if (urlPortada == "")
            urlPortada = null;
          if (valoracion == "")
            valoracion = null;

          peliculaRepository.save(new Pelicula(nombrePelicula, urlPelicula, sinopsis, ano,
                  director, reparto, urlPortada, valoracion));
        }
      } catch (NonTransientDataAccessException e) {
        if (e.getCause() instanceof ConstraintViolationException) {
          mensaje = "Error: Pelicula ya ha sido introducida existe.";
          color = "color:#D20F0F";
        } else throw e;
      }
    }
    return new ModelAndView("newMovie")
            .addObject("mensaje",mensaje).addObject("color",color);
  }



  @CacheEvict(value ="principal",allEntries=true)
  @Secured({"ROLE_ADMIN"})
  @RequestMapping("/administrarPeliculas/{id}")
  public ModelAndView processModMovie(@PathVariable("id") String idPelicula,
                                      @RequestParam(value="nombrePelicula", required = false) String nombrePelicula,
                                      @RequestParam(value="urlPelicula", required = false) String urlPelicula,
                                      @RequestParam(value="sinopsis", required = false) String sinopsis,
                                      @RequestParam(value="ano", required = false) String ano,
                                      @RequestParam(value="director", required = false) String director,
                                      @RequestParam(value="reparto", required = false) String reparto,
                                      @RequestParam(value="urlPortada", required = false) String urlPortada,
                                      @RequestParam(value="valoracion", required = false) String valoracion,
                                      @RequestParam(value="intento", required = false) String intento){

    String mensaje = "";
    String color = "color:#07BD1D";


    Pelicula pelicula = peliculaRepository.findOne(Long.parseLong(idPelicula));

    //Verificamos si ha intentado introducir datos
    if(intento!=null) {
      mensaje = "Pelicula modificada correctamente.";
      color = "color:#07BD1D";


      if (ano == "")
        ano = null;
      else {
        try {
          Integer.parseInt(ano);
        } catch (NumberFormatException e) {
          mensaje = "Error: El campo año introducido no es un numero.";
          color = "color:#D20F0F";

          return new ModelAndView("modMovie").addObject("mensaje", mensaje)
                  .addObject("color", color).addObject("pelicula", pelicula);
        }
      }

      if (nombrePelicula == "" || urlPelicula == "") {
        mensaje = "Error: No se han introducido los campos obligatorios.";
        color = "color:#D20F0F";
      } else {
        if (sinopsis == "")
          sinopsis = null;
        if (director == "")
          director = null;
        if (reparto == "")
          reparto = null;
        if (urlPortada == "")
          urlPortada = null;
        if (valoracion == "")
          valoracion = null;

        pelicula.setNombre(nombrePelicula);
        pelicula.setUrlPelicula(urlPelicula);
        pelicula.setSinopsis(sinopsis);
        pelicula.setAno(ano);
        pelicula.setDirector(director);
        pelicula.setReparto(reparto);
        pelicula.setUrlPortada(urlPortada);
        pelicula.setValoracion(valoracion);

        peliculaRepository.save(pelicula);

        return new ModelAndView("modMovie").addObject("mensaje", mensaje)
                .addObject("color", color).addObject("pelicula", pelicula);
      }
    }


    return new ModelAndView("modMovie").addObject("mensaje",mensaje)
            .addObject("color",color).addObject("pelicula",pelicula);
  }

  @CacheEvict(value ="principal",allEntries=true)
  @Secured({"ROLE_ADMIN"})
  @RequestMapping("/administrarPeliculas/{id}/borrada")
  public ModelAndView processDeleteMovie(@PathVariable("id") Long idPelicula){

    Pelicula pelicula = peliculaRepository.findOne(idPelicula);
    peliculaRepository.delete(pelicula);

    return new ModelAndView("eliminarMovie");
  }

  @Secured({ "ROLE_USER"})
  @RequestMapping("/pelicula/{input}" )
  public ModelAndView pelicula (@PathVariable("input") String input) {

    Pelicula p = peliculaRepository.findOne(Long.parseLong(input));
    boolean admin = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().contains("ROLE_ADMIN");

    int valoracion = 0;

    RestAdapter adapter = new RestAdapter.Builder().setEndpoint( "https://api.themoviedb.org" ).build();
    ServicioBusqueda service = adapter.create(ServicioBusqueda. class);
    MovieList obj=service.getList(p.getNombre());


    if(obj.getTotalResults()!=0) {

      MovieDetails details = service.getDetails(obj.getResults().get(0).getId());

      Credits credits =service.getCredits(obj.getResults().get(0).getId());

      List<Cast> castMembers = credits.getCast();
      List<Crew> crewMembers = credits.getCrew();

      if (details.getOverview()!=null && (p.getSinopsis() == null || p.getSinopsis() == "" || p.getSinopsis().length() == 1 || p.getSinopsis().length() == 0)) {
        p.setSinopsis(details.getOverview());
      }
      if (details.getPosterPath()!=null && (p.getUrlPortada() == null || p.getUrlPortada() == "" || p.getUrlPortada().length() == 1 || p.getUrlPortada().length() == 0)) {
        p.setUrlPortada("http://image.tmdb.org/t/p/w780" + details.getPosterPath());
      }
      Boolean encontrado = false;
      int i = 0;

      //Recorremos la lista del equipo de produccion buscando el director
      while (!encontrado && crewMembers.size() > i) {

        if (crewMembers.get(i).getJob().equals("Director") ) {
          encontrado = true;
          if(p.getDirector()==null || p.getDirector()=="") {
            p.setDirector(crewMembers.get(i).getName());
          }
        }
        i++;
      }

      i = 0;

      //Recorremos el reparto añadiendo los actores, maximo 20
      if(p.getReparto()==null || p.getReparto()=="") {
        while (castMembers.size() > i && i < 20) {
          if (i != 0) {
            p.setReparto(p.getReparto() + ", ");
          }
          p.setReparto((p.getReparto() != null ? p.getReparto() : "") + castMembers.get(i).getName());
          i++;
        }
      }

      if (details.getReleaseDate()!=null && (p.getAno() == null)) {
        p.setAno(details.getReleaseDate().substring(0, 4));
      }
      if ((p.getValoracion() == null || p.getValoracion() == "" || p.getValoracion().length() == 1 || p.getValoracion().length() == 0)) {
        p.setValoracion("" + details.getVoteAverage());
        try {
          valoracion = Integer.parseInt(p.getValoracion().substring(0,1));
        } catch (NumberFormatException e) {
          System.out.println("Sale incorrecto");
          return new  ModelAndView("pelicula").addObject("pelicula",p)
                  .addObject("admin",admin).addObject("val",0);
        }
      }
    }

    return new  ModelAndView("pelicula").addObject("pelicula",p)
            .addObject("admin",admin).addObject("val",valoracion);
  }*/
}


